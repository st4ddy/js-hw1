// let - значение переменной можно менять, const - значение менять будет нельзя
// var - устаревший метод обьявления переменных

const userName = prompt('Your name?');
const userAge = +prompt('Your age?');

if (userAge < 18) {
alert('You are not allowed to visit this website');
}
else if (userAge >= 18 && userAge <= 22) {
    const accept = confirm('Are you sure you want to continue?');
    if (accept === true) {
        alert(`Welcome ${userName}.`);
    }
    else {
        alert('You are not allowed to visit this website');
    }
}
else {
    alert(`Welcome ${userName}.`);
}